# ag_treebanked_trees

This repository gathers Ancient Greek texts annotated following the
the Ancient Greek Dependency Guidelines.

* The list of the original texts can be found in `/original_texts`
* An attempt to automatically normalize them is contained in `/normalized_texts`
