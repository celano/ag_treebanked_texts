# Normalized Texts

The texts in `merge` have been normalized for machine learning so:

* The form and lemmas of the elliptical nodes have been converted into ordered `[0]`, 
`[1]`, depending on the position of the elliptical nodes. Before normalization,
some elliptical nodes have form and/or lemma specified, and the use of `[0]`,
`[1]` did not always followed the order (i.e., `[0]` was not always the first,
`[1]` the second, and so on and so forth).
