The original files in the `merge` directory come from:

- The Ancient Greek Dependency Treebank 2.1
(https://github.com/PerseusDL/treebank_data/releases/tag/v2.1_IGDS)
- The Gorman Trees (https://github.com/vgorman1/Greek-Dependency-Trees). The
texts derive from the master branch downloaded in August 2022 (indeed, 
the available releases are older and contain fewer texts)
- The Pedalion Trees (https://github.com/perseids-publications/pedalion-trees/tree/master/public/xml)
- Yordanova's Aphthonius ()https://github.com/polinayordanova/Treebank-of-Aphtonius-Progymnasmata)
- Daphne (https://github.com/francescomambrini/Daphne/releases/tag/v0.4.1-agldt)

The file names have been changed, so that all of them follow the schema
tlgXXXX.tlgXXX.

The above-mentioned data are the known available one annotated
according to the Ancient Greek Dependency Treebank annotation scheme.
