(: BaseX 10.7 :)

(: merge all texts deriving from Perseus, Gorman, and Pedalion, Yordanova  
   treebanks :)
copy $t := 

<treebank>{collection("../original_texts/merge")//sentence}</treebank>

modify
(
  for $s at $count in $t//sentence
  return
  (insert node attribute absolute_id {$count} into $s,
   insert node attribute text {$s//word/@form} into $s) 
)
return
file:write("../original_texts/all_texts_of_merge.xml", $t, 
            map {"indent": "yes"})
