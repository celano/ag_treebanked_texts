(: BaseX 10.7:)

declare variable $path := "../original_texts/all_texts_of_merge.xml";

copy $r := doc($path)
modify(
  for $x in $r//sentence
  for $w in $x//word
  (: a sentence contains an id like 34,1; ids should be integers:)
  where $w[contains(@id, ",")]
  return
  delete node $x
      )
return
file:write($path, $r, map {"indent":"yes"})
