(: BaseX 10.7:)

copy $r := doc("../normalized_texts/all_sentences_greek.xml")
modify(
  for $x in $r//sentence[.//word/@head = "&quot;&quot;"]
  return
  delete node $x
)
return 

(: the following is to delete a few problematic sentences 
   mostly in pedalion texts 
:)
copy $f := $r
modify
(
for $w in $f//word
where $w/@id = ('', "34,1")
return
delete node $w  
)
return

copy $h := $f
modify(
for $s in $h//sentence
where $s//word/@postag/substring(., 1,1) = ("z", "δ")
return
delete node $s  
)
return 

copy $a := $h
modify(
for $w in $a//sentence//word
where $w/@postag/substring(., 1,1) = "-"
return
replace value of node $w/@postag with 
       "x" || substring($w/postag, 2) 
  
)
return

copy $q := $a
modify
(
for $w in $q//word[substring(@postag, 1,1) = "b"]
let $so := doc("frequencies.xml")//g[@f = $w/@form || "_" || $w/@lemma]/j[1]  
return
if ($so) then
 replace value of node $w/@postag with $so/@v
else
 replace value of node $w/@postag with "x--------"  
)
return 

copy $z := $q
modify(
for $w in $z//sentence//word
where $w/@postag = "x"
return
replace value of node $w/@postag with 
       "x--------" 
)
return

copy $c := $z
modify(
for $w in $c//sentence//word
where count(analyze-string($w/@postag, ".")//*) > 9
return
replace value of node $w/@postag with 
       substring($w/@postag, 1,9)
)
return

copy $w := $c
modify(
for $s in $w//sentence
where $s//word[count(analyze-string(@postag, ".")//*) < 9]
return
delete node $s
)
return

copy $i := $w (: a mistake :)
modify(
for $t in $i//sentence//word[@postag = "v-spaafn-"]
return
replace value of node $t/@postag with "v-sppafn-"
  
)
return 

copy $x := $i (: a mistake :)
modify(
for $t in $x//sentence//word[@postag = "v3said---"]
return
replace value of node $t/@postag with "v3saim---"
  
)
return 

copy $z := $x (: a mistake :)
modify(
for $t in $z//sentence//word[@postag = "v--pnd---"]
return
replace value of node $t/@postag with "v--pnm---"
  
)
return 


copy $j := $z (: positive adjective as - :)
modify(
for $t in $j//sentence//word[substring(@postag, 9,1) = "p"]
return
replace value of node $t/@postag with substring($t/@postag, 1,8) || "-"
  
)
return 

copy $r := $j
modify(
for $s in $r//sentence
where $s//word[@relation = ""]
return
delete node $s
  
)
return 

copy $j := $r
modify(
for $t in $j//sentence//word[@relation = "_"]
return
replace value of node $t/@relation with  "PRED"
)
return 

copy $m := $j 
modify(
for $t in $m//sentence//word[@relation = "!!!!!!ATR"]
return
replace value of node $t/@relation with  "ATR"
)
return 

copy $m := $m
modify(
for $s in $m//sentence
where $s//word[@relation = ("AP", "PUNCT", "Aux", "AUX")]
return
delete node $s
)
return 

copy $o := $m 
modify(
for $t in $o//sentence//word[@relation = "Auxy"]
return
replace value of node $t/@relation with  "AuxY"
)
return 

copy $s := $o 
modify(
for $t in $s//sentence//word[@relation = "MWE2"]
return
replace value of node $t/@relation with  "MWE"
)
return 

copy $d := $s
modify(
for $t in $d//sentence//word[@relation = ("Aucz", "AucZ")]
return
replace value of node $t/@relation with  "AuxZ"
)
return 

copy $x := $d
modify(
for $t in $x//sentence//word[@relation = "CONJ"]
return
replace value of node $t/@relation with  "AuxC"
)
return 


copy $x := $x (: only one specific case :)
modify(
for $t in $x//sentence//word[@form = ""]
return
replace value of node $t/@form with  $t/@lemma
)
return 


copy $x := $x (: only one specific case :)
modify(
for $t in $x//sentence//word[@form => string-to-codepoints() = 788] (: 700 ?:)
return
(replace value of node $t/@form with  "‘",
replace value of node $t/@lemma with  "‘")
)
return

copy $x := $x (: 14 cases :)
modify(
for $s in $x//sentence
for $t in $s//word
where contains($t/@form, " ")
return
delete node $s
)
return

copy $x := $x (: 1 case :)
modify(
for $s in $x//sentence
where matches($s/@text, " $")
return
delete node $s
)
return

copy $x := $x (: 1 case :)
modify(
for $s in $x//sentence
for $w in $s//word
where contains($w/@lemma, " ")
return
delete node $s
)
return

copy $r := $x
modify(
  for $x in $r//sentence
  (: the following create an issue with BERT tokenizers: 
     text should not contain prosodic markup :)
  where $x[contains(@text, "¯")] or $x[contains(@text, " ̆ ")]
  return
  delete node $x
      )
return
file:write("../normalized_texts/all_sentences_greek.xml", $r,
           map {"indent":"yes"})
