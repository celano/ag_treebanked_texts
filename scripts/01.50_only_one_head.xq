(: BaseX 10.7 :)

declare variable $path := "../normalized_texts/all_sentences_greek.xml";

copy $d := doc($path)
modify (
for $y in $d//sentence
let $pre := if ($y//word[@relation="PRED"][@head="0"]) then
            $y//word[@relation="PRED"][@head="0"][1] else $y//word[@head="0"][1]
where count($y//word[@head="0"]) > 1
return
  for $x in $y//word[@head="0"]
  where $x/@id != $pre/@id
  return
  replace value of node $x/@head with $pre/@id 
)
return
 
(: the following is to delete one erroneous sentence:)
copy $f := $d
modify (
for $x in $f//sentence
where count($x//word[@head="0"]) > 1
return
delete node $x  
)
return
file:write($path, $f, map {"indent":"yes"})
