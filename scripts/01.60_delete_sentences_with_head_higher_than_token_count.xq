(: BaseX 10.7 :)

(: delete sentences where a head value is higher than the numbers of sentence
words:)

declare variable $path := "../normalized_texts/all_sentences_greek.xml";

copy $text:=  doc($path)
modify(
for $s in $text//sentence
let $count := count($s/word)
let $values
 := distinct-values(for $t in $s/word
                          where xs:integer($t/@head) > $count
                          return
                          $s/@absolute_id)
where $s/@absolute_id = $values
return
delete node $s
)
return
file:write($path, $text, map {"indent":"yes"})
