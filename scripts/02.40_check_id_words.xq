(: BaseX 10.7 :)

declare variable $path := 
"../normalized_texts/all_sentences_greek.xml";

let $values := distinct-values(for $s in doc($path)//s
for $w at $c in $s//t
return
if ($c != xs:integer($w/@id))
then $s/@absolute_id else ())
return

copy $text := doc($path) 
modify (
for $s in $text//s
where $s/@absolute_id = $values 
return
delete node $s 
)
return
file:write($path, $text, map {"indent":"yes"})
