from lxml import etree

path = "../normalized_texts/all_sentences_greek.xml"

tree = etree.parse(path)

root = tree.getroot()
el_sent = root.xpath(".//s")

def find_sentences_with_cycles(el_sent):
  sentence_ids = []
  for n0, s in enumerate(el_sent):
      words = s.xpath(".//t")
      for w in words:
       id_ =  w.attrib["id"]
       head = int(w.attrib["head"])
       count = 0
       while head != 0:
         new = s[head - 1]
         id_ = new.attrib["id"]
         head = int(new.attrib["head"])
         count += 1
         if count > len(s):
           sentence_ids.append(s.attrib["absolute_id"])
           print(f"cycle at {s.attrib['absolute_id']}, word id: {id_}, " +
                 f"doc id: {s.attrib['doc_id']}")
           break
  return list(set(sentence_ids)) 

sentence_ids = find_sentences_with_cycles(el_sent)

for n0, s in enumerate(root.xpath(".//s")):
    if s.attrib["absolute_id"] in sentence_ids:
      s.getparent().remove(s)

new_file = etree.tostring(root, encoding="utf8").decode("utf8")
with open("../normalized_texts/all_sentences_greek.xml", "w") as f:
  print(new_file, file=f) 

