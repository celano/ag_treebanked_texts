(: BaseX 10.7 :)

declare variable $p := 
doc("../normalized_texts/all_sentences_greek.xml");


file:write-text("../normalized_texts/all_sentences_greek.conllu", 


string-join(
for $s in $p//s
return

string-join(
  ("# absolute_id = " || $s/@absolute_id,
  "# doc_id = " || $s/@doc_id,
 "# sent_id = " || $s/@id,
  "# text = " || $s/@text,

for $t in $s//t

return

string-join(($t/@id, $t/@form, $t/@lemma, 


if (matches($t/@form, "\[\p{N}*\]")) then "-" else substring($t/@postag, 1,1), 

if (matches($t/@form, "\[\p{N}*\]")) then "---------" else $t/@postag,


string-join
(
if (matches($t/@form, "\[\p{N}*\]")) then "_"

else

let $r := analyze-string($t/@postag, ".")//*[position() > 1]
return 

 (if ($r[7] != "-")  then "Case=" || $r[7],
  if ($r[8] != "-")  then "Degree=" || $r[8],
  if ($r[6] != "-")  then "Gender=" || $r[6],
  if ($r[4] != "-")  then "Mood=" || $r[4],
  if ($r[2] != "-")  then "Number=" || $r[2],
  if ($r[1] != "-")  then "Person=" || $r[1],
  if ($r[3] != "-")  then "Tense=" || $r[3],
  if ($r[5] != "-")  then "Voice=" || $r[5],
  if (matches($t/@postag, ".--------")) then "_"
  ), 
  
  "|")  
,
$t/@head, 
$t/@relation, "_", "_"), 
"	") 
)

, "&#xA;")

,

"&#xA;&#xA;"
) 

||

(: the following is the final newline characters :)

"&#xA;&#xA;" 
) 
